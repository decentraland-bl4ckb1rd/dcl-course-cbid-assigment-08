
const point1 = new Vector3(6.7, 0.2, 8);
const point2 = new Vector3(9, 0.1, 9);
const point3 = new Vector3(8, 0.2, 6.7);
const path: Vector3[] = [point1, point2, point3];

@Component('lerpData')
class LerpData {
    array: Vector3[] = path;
    origin: number = 0;
    target: number = 1;
    fraction: number = 0;
}

export default class Dino extends Entity {
    public animationOn: boolean;

    constructor(scene: Entity) {
        super();
        engine.addEntity(this);
        engine.addSystem(this);
        this.setParent(scene);

        const audioClipDino = new AudioClip('sounds/trex-roar.mp3');

        this.addComponent(new GLTFShape("models/trex.glb"));
        this.addComponent(new Transform({
            position: new Vector3(6.7, 0.2, 8),
            rotation: Quaternion.Euler(0, 90, 0),
            scale: new Vector3(0.25, 0.25, 0.25)
        }));
        this.addComponent(new AudioSource(audioClipDino));
        this.addComponent(new Animator());
        this.getComponent(Animator).addClip(new AnimationState("TRex_Idle"));
        this.getComponent(Animator).addClip(new AnimationState("TRex_Run"));
        this.addComponent(new LerpData());
        this.addComponent(
            new OnClick(e => {
                this.animationOn = this.animationOn === true ? false : true;
                if (this.animationOn) {
                    this.getComponent(Animator).getClip("TRex_Idle").pause();
                    this.getComponent(Animator).getClip("TRex_Run").play();
                    this.getComponent(AudioSource).playing = true;
                    this.getComponent(AudioSource).loop = true;
                } else {
                    this.getComponent(Animator).getClip("TRex_Idle").play();
                    this.getComponent(Animator).getClip("TRex_Run").pause();
                    this.getComponent(AudioSource).playing = false;
                }
            }) 
        );
        this.getComponent(Animator).getClip("TRex_Idle").play();
    }

    update(dt: number) {
        this.walk(dt);
    }

    walk(dt: number): void {
        if (this.animationOn) {
            let transform = this.getComponent(Transform);
            let path = this.getComponent(LerpData);
            path.fraction += dt / 3;
            if (path.fraction < 1) {
                transform.position = Vector3.Lerp(
                    path.array[path.origin],
                    path.array[path.target],
                    path.fraction
                );
            } else {
                path.origin = path.target;
                path.target += 1;
                if (path.target >= path.array.length) {
                    path.target = 0
                }
                path.fraction = 0;
                transform.lookAt(path.array[path.target]);
            }
        }
    }
}