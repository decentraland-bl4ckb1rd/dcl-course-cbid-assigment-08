import Dino from "./dino";

let scene = new Entity();
let sceneTransform = new Transform({
    position: new Vector3(16, 0, 16),
    rotation: Quaternion.Euler(0, 180, 0)
});
scene.addComponent(sceneTransform);
engine.addEntity(scene);

let landscape = new Entity();
let landscapeShape = new GLTFShape("models/landscape.glb");
let landscapeTransform = new Transform({
    position: new Vector3(8.6, 0, 7.47),
    rotation: Quaternion.Euler(0, 0, 0),
    scale: new Vector3(0.65, 0.65, 0.61)
});
landscape.addComponent(landscapeShape);
landscape.addComponent(landscapeTransform);
landscape.setParent(scene);
engine.addEntity(landscape);

let foliage = new Entity();
let foliageShape = new GLTFShape("models/foliage.glb");
let foliageTransform = new Transform({
    position: new Vector3(0.5, 0.8, 11.2),
    rotation: Quaternion.Euler(-90, 0, 180),
    scale: new Vector3(2.6, 0.7, 0.2)
});
foliage.addComponent(foliageShape);
foliage.addComponent(foliageTransform);
foliage.setParent(scene);
engine.addEntity(foliage);

let dino = new Dino(scene);
