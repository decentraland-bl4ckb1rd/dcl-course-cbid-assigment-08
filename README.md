# Dino Land a decentraland experiment!

Create for the course "Building in Decentraland" a Decentraland University VR Academy Course.

To see the creation:

* <https://dcl-dino-land.bl4ckb1rd.now.sh/?position=-142%2C-87>
* <https://explorer.decentraland.org/?position=-142%2C-87>

## Models and sounds sources

### Models

* The land extracted from model "mechanical T Rex with animation" make by Freddy drabble
  * <https://sketchfab.com/3d-models/mechanical-t-rex-with-animation-87b01edef443419bb4bc9899b4838a82>
* The dinosaur patreon a Quaternius
  * <https://www.patreon.com/quaternius>
* The foliage maked by me.

### Sound

* The t-rex roar download from freesound make by CGEffex
  * <https://freesound.org/people/CGEffex/sounds/96223/?page=4#>
